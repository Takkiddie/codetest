<?php
//Ian Scarborough


/**
 * //Simply executes a raw query and returns the mysqli_result
 * @return bool|mysqli_result
 */
function getQueryResults() {
    $con=mysqli_connect("127.0.0.1","root","","testdata");
    // Check connection
    if (mysqli_connect_errno())
    {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }

    $select = '
    jobs.name as job,
    applicants.name as applicant,
    applicants.email as email,
    applicants.website as website,
    applicants.cover_letter as cover_letter,


    skills.name as skill,
    jobs.id as job_id,
    applicants.id as applicant_id
    ';

    $query = "
    SELECT $select FROM jobs
    LEFT JOIN applicants ON applicants.job_id = jobs.id
    LEFT JOIN skills ON skills.applicant_id = applicants.id
    ORDER BY jobs.id, applicants.id 
    ";

    // Perform queries
    $results = mysqli_query($con,$query);

    mysqli_close($con);
    return $results;
}

/**
 * Gets the 'row spans' attribute for each of the entities in the return value
 * It then puts them all into a single array, organized by the attribute id
 *
 * @param $ids
 * @param $results
 * @return array
 */
function getRowSpans($ids,$results) {
    $row_spans = [];

    //For each entity id, we will need an array
    foreach($ids as $id) {
        $row_spans[$id] = [];
    }

    //For every row returned
    foreach ($results as $result) {
        //Check every id in question, and count every instance
        foreach($ids as $id) {
            if (!isset($row_spans[$id][$result[$id]])) {
                $row_spans[$id][$result[$id]] = 0;
            }
            //Count every time a particular ID shows up.
            //That is how many rows it's block will cover in the end result.
            $row_spans[$id][$result[$id]] += 1;
        }
    }
    return $row_spans;
}

/**
 * @param $row
 * @param $row_spans
 * @param $name
 * @param $id
 * @return string
 */
function buildTableCell($row,&$row_spans,$name,$id) {
    $ret = '';

    //If spans is set to Zero, row has already been shown. Do not show again.
    if($row_spans[$id][$row[$id]] != 0) {
        $ret .= '<td rowspan="'.$row_spans[$id][$row[$id]].'">';
        $ret .= $row[$name];
        $ret .= '</td>';
    }
    return $ret;
}

/**
 * Performs the main service of
 * @return string
 */
function main() {
    $ret = '';
    $results = getQueryResults();
    $ids = ['job_id','applicant_id'];

    $row_spans = getRowSpans($ids,$results);

    //For Each Result
    //Build one row.
    foreach ($results as $row) {
        $ret .= '<tr>';
        //Only make one job row for every job.
        $ret .=  buildTableCell($row,$row_spans,'job','job_id');
        $row_spans['job_id'][$row['job_id']] = 0;

        //Only make one of each of these for each applicant
        $ret .= buildTableCell($row,$row_spans,'applicant','applicant_id');
        $ret .= buildTableCell($row,$row_spans,'email','applicant_id');
        $ret .= buildTableCell($row,$row_spans,'website','applicant_id');

        $ret .= '<td>';
        $ret .= $row['skill'];
        $ret .= '</td>';

        $ret .= buildTableCell($row,$row_spans,'cover_letter','applicant_id');

        $row_spans['applicant_id'][$row['applicant_id']] = 0;
        $ret .= '</tr>';
    }
    return $ret;
}
?>

<head>
    <title>Job Applicants Report</title>
    <link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.9.1/build/cssreset/cssreset-min.css">
    <link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.9.1/build/cssbase/cssbase-min.css">
    <style type="text/css">
        #page {
            width: 1200px;
            margin: 30px auto;
        }
        .job-applicants {
            width: 100%;
        }
        .job-name {
            text-align: center;
        }
        .applicant-name {
            width: 150px;
        }
    </style>
</head>


<body>

<table class='job-applicants'>
    <tr>
        <th>Job</th>
        <th>Applicant Name</th>
        <th>Email Address</th>
        <th>Website</th>
        <th>Skills</th>
        <th>Cover Letter Paragraph</th>
    </tr>

    <?php
    echo main();
    ?>

</table>

</body>