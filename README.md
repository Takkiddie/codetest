# README #

This is a quick, framework-independant version of the assignment I was sent. It's designed to be light weight. Only requiring a database, and a php installation.

It's designed to be as small and self-containing as possible.

### What is this repository for? ###

* Quick summary - Builds a table from a database
* Version 1.0

### How do I get set up? ###

Just download the PHP and input the correct credentials in the code.

If index.php is running, the rest should run fine.

### Contribution guidelines ###

### Who do I talk to? ###

Ian Scarborough - The intended receiver of this application should have my contact information already.
